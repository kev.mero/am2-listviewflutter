import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mail_app/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Crea nuestra aplicación y activa un marco.
    await tester.pumpWidget(const MyApp());

    // Verificamos que nuestro contador comience en 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Toque el ícono '°' y active un cuadro.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verificar que nuestro contador se haya incrementado.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
}
